const express = require('express');
const serveStatic = require('serve-static');
const history = require('connect-history-api-fallback');
const path = require('path');

const app = express();
const port = process.env.PORT || 3000;
const staticFileMiddleware = serveStatic(path.join(__dirname, 'dist'));

app.use(staticFileMiddleware);
app.use(history({
  disableDotRule: true,
  verbose: true,
}));
app.use(staticFileMiddleware);
app.listen(port, () => console.log(`App is being exposed on port ${port}`));
