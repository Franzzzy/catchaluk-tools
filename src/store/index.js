import { createLogger, createStore } from 'vuex';

const store = createStore({
  state: () => ({
    backgroundImageDimensions: {
      width: 2444,
      height: 3455,
      get ratio() {
        return this.width / this.height;
      },
      leftColumn: 174,
      rightColumn: 168,
      topRow: 114,
      bottomRow: 118,
      get innerRectangle() {
        return {
          width: this.width - this.leftColumn - this.rightColumn,
          height: this.height - this.topRow - this.bottomRow,
        };
      },
    },
    screenSize: {
      width: 0,
      height: 0,
    },
    pageAtoms: {
      0: { height: 0, gapHeight: 0, gapMessage: { showMessage: false, availableSpace: 0 } },
      1: { height: 0, gapHeight: 0, gapMessage: { showMessage: false, availableSpace: 0 } },
      2: { height: 0, gapHeight: 0, gapMessage: { showMessage: false, availableSpace: 0 } },
      3: { height: 0, gapHeight: 0, gapMessage: { showMessage: false, availableSpace: 0 } },
      4: { height: 0, gapHeight: 0, gapMessage: { showMessage: false, availableSpace: 0 } },
      5: { height: 0, gapHeight: 0, gapMessage: { showMessage: false, availableSpace: 0 } },
      6: { height: 0, gapHeight: 0, gapMessage: { showMessage: false, availableSpace: 0 } },
      7: { height: 0, gapHeight: 0, gapMessage: { showMessage: false, availableSpace: 0 } },
    },
    additionalPagesNeedByAtom: {},
  }),
  getters: {
    additionalPagesNb(state) {
      return Object.values(state.additionalPagesNeedByAtom).reduce((acc, curr) => acc + curr, 0);
    },
    pageNb(state, getters) {
      return Object.values(state.pageAtoms).filter((page) => page.gapHeight !== 0).length
        + 1 + getters.additionalPagesNb;
    },
    fullPageSize(state) {
      const screenWidth = state.screenSize.width;
      return {
        width: screenWidth,
        height: screenWidth / state.backgroundImageDimensions.ratio,
      };
    },
    getRelativeLength: (state, getters) => (axis, length) => length
      * (getters.fullPageSize[axis] / state.backgroundImageDimensions[axis]),
    relativeDimensions: (state, getters) => ({
      backgroundWidth: `${getters.getRelativeLength('width', state.backgroundImageDimensions.width)}px`,
      displayPagesHeight: `${
        getters.getRelativeLength('height', state.backgroundImageDimensions.innerRectangle.height)}px`,
      displayPagesMarginLeft: `${getters.getRelativeLength('width', state.backgroundImageDimensions.leftColumn)}px`,
      displayPagesMarginBottom: `${getters.getRelativeLength('height', state.backgroundImageDimensions.bottomRow)}px`,
      displayPagesMarginTop: `${getters.getRelativeLength('height', state.backgroundImageDimensions.topRow)}px`,
      displayPagesWidth: `${
        getters.getRelativeLength('width', state.backgroundImageDimensions.innerRectangle.width)}px`,
    }),
  },
  mutations: {
    updateScreenSize(state, { width, height }) {
      state.screenSize.width = width;
      state.screenSize.height = height;
    },
    updatePageAtomSize(state, { height, pageAtomIndex }) {
      state.pageAtoms[pageAtomIndex].height = height;
    },
    updatePageAtomGap(state, { gapHeight, pageAtomIndex }) {
      state.pageAtoms[pageAtomIndex].gapHeight = gapHeight;
    },
    updatePageAtomShowMessage(state, { gapHeight, pageAtomIndex, remainingAvailableSpace }) {
      const showMsg = Boolean(gapHeight && remainingAvailableSpace >= 76);
      state.pageAtoms[pageAtomIndex].gapMessage.showMessage = showMsg;
      state.pageAtoms[pageAtomIndex].gapMessage.availableSpace = showMsg ? remainingAvailableSpace : 0;
    },
    addNeededPages(state, { pageIndex, pageNb }) {
      state.additionalPagesNeedByAtom[pageIndex] = pageNb;
    },
    resetAdditionalNeededPages(state) {
      Object.keys(state.additionalPagesNeedByAtom).forEach((k) => delete state.additionalPagesNeedByAtom[k]);
    },
  },
  actions: {
    commitPagesUpdates({ commit }, { gapHeight, pageAtomIndex, remainingAvailableSpace }) {
      commit('updatePageAtomGap', { gapHeight, pageAtomIndex });
      commit('updatePageAtomShowMessage', { gapHeight, pageAtomIndex, remainingAvailableSpace });
    },
    updatePagesLayout({ commit, dispatch, getters, state }) {
      commit('resetAdditionalNeededPages');
      let acc = 0;
      Object.entries(state.pageAtoms).forEach(([index, atom]) => {
        const pageHeight = getters.getRelativeLength('height', state.backgroundImageDimensions.innerRectangle.height);
        const remainingAvailableSpace = pageHeight - acc;
        if (atom.height > remainingAvailableSpace) {
          // update gap
          const interPageHeight = getters.getRelativeLength('height', state.backgroundImageDimensions.bottomRow)
            + getters.getRelativeLength('height', state.backgroundImageDimensions.topRow);
          const gapHeight = remainingAvailableSpace + interPageHeight;
          dispatch('commitPagesUpdates', { gapHeight, pageAtomIndex: index, remainingAvailableSpace });
          if (pageHeight && atom.height > pageHeight + interPageHeight) {
            // update acc
            const pageNb = parseInt(atom.height / pageHeight, 10);
            acc = (atom.height - (interPageHeight * pageNb)) % pageHeight;
            // additional pages
            commit('addNeededPages', { pageIndex: index, pageNb });
          } else {
            // update acc
            acc = atom.height;
          }
        } else {
          if (atom.gapHeight !== 0) {
            dispatch('commitPagesUpdates', { gapHeight: 0, pageAtomIndex: index, remainingAvailableSpace });
          }
          acc += atom.height;
        }
      });
    },
  },
  modules: {
  },
  plugins: process.env.NODE_ENV !== 'production' ? [createLogger()] : [], // print logs in console (if not in prod)
});

export default store;
