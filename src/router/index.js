import { createRouter, createWebHistory } from 'vue-router';
import V1 from '../views/V1.vue';

const routes = [
  {
    path: '/',
    name: 'V1',
    component: V1,
  },
  // {
  //   path: '/v2',
  //   name: 'V2',
  //   // route level code-splitting
  //   // this generates a separate chunk (v2.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "v2" */ '../views/V2.vue'),
  // },
  // N.B: Keep this next one the last one of the list
  // {
  //   path: '/:catchAll(.*)',
  //   name: 'Error404',
  //   // route level code-splitting
  //   // this generates a separate chunk (error404.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "error404" */ '../views/Error404.vue'),
  // },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
