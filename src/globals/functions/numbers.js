/* eslint-disable no-bitwise */
const floorRoundUp = (nb) => ~~nb; // A better performing alternative to Math.floor
const randomInt = (max) => floorRoundUp(Math.random() * max);
const range = (end, start = 0, step = 1) => Array.from({ length: Math.ceil((end - start) / step) },
  (_, i) => start + i * step);

export { floorRoundUp, randomInt, range };
