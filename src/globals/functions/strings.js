import { range } from './numbers';

const findAllIndexes = (st, ch) => range(st.length).reduce((acc, index) => {
  if (st[index] === ch) {
    acc.push(index);
  }
  return acc;
}, []);

export default findAllIndexes;
